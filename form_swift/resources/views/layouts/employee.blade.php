 <script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<!-- jQuery easing plugin -->
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
	   <meta charset="utf-8">

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 <style>
 
#msform {
	width: 100%;
	margin: 50px auto;
	text-align: center;
	position: relative;
}
#msform fieldset {
	background: white;
	border: 0 none;
	border-radius: 3px;
	box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
	padding: 20px 30px;
	
	box-sizing: border-box;
	width: 80%;
	margin: 0 10%;
    margin-top:-32px;
	
	/*stacking fieldsets above each other*/
	position: absolute;
}
/*Hide all except first fieldset*/
#msform fieldset:not(:first-of-type) {
	display: none;
}
/*inputs*/
#msform input, #msform textarea {
	padding: 15px;
	border: 1px solid #ccc;
	border-radius: 3px;
	margin-bottom: 10px;
	width: 100%;
	box-sizing: border-box;
	font-family: montserrat;
	color: #2C3E50;
	font-size: 13px;
}
/*buttons*/
#msform .action-button {
	width: 100px;
	background: #27AE60;
	font-weight: bold;
	color: white;
	border: 0 none;
	border-radius: 1px;
	cursor: pointer;
	padding: 10px 5px;
	margin: 10px 5px;
}
#msform .action-button:hover, #msform .action-button:focus {
	box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;
}
/*headings*/
.fs-title {
	font-size: 15px;
	text-transform: uppercase;
	color: #2C3E50;
	margin-bottom: 10px;
}
.fs-subtitle {
	font-weight: normal;
	font-size: 13px;
	color: #666;
	margin-bottom: 20px;
}
/*progressbar*/
#progressbar {
	margin-bottom: 30px;
	overflow: hidden;
	/*CSS counters to number the steps*/
	counter-reset: step;
}
#progressbar li {
	list-style-type: none;
	color: white;
	text-transform: uppercase;
	font-size: 9px;
	width: 20%;
	float: left;
	position: relative;
}
#progressbar li:before {
	content: counter(step);
	counter-increment: step;
	width: 20px;
	line-height: 20px;
	display: block;
	font-size: 10px;
	color: #333;
	background: white;
	border-radius: 3px;
	margin: 0 auto 5px auto;
}
/*progressbar connectors*/
#progressbar li:after {
	content: '';
	width: 100%;
	height: 2px;
	background: white;
	position: absolute;
	left: -50%;
	top: 9px;
	z-index: -1; /*put it behind the numbers*/
}
#progressbar li:first-child:after {
	/*connector not needed before the first step*/
	content: none; 
}
/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
#progressbar li.active:before,  #progressbar li.active:after{
	background: #27AE60;
	color: white;
}

 .hide{
 display:none;
 }

/**** Animation for fades ****/
@-webkit-keyframes animate_in {
    from {right:-300px; opacity:0}
    to {right:15; opacity:1}
}

@keyframes animate_in {
    from {right:-300px; opacity:0}
    to {right:15; opacity:1}
}

@-webkit-keyframes animate_out {
    from {right:15; opacity:1}
    to {right:-300px; opacity:0}
}

@keyframes animate_out {
    from {right:15; opacity:1}
    to {right:-300px; opacity:0}
}

/**** Wrapper styles ****/
 

/**** Button styles ****/
 

/**** Alert message styles ****/
#alert {
  display: none;
  position: absolute;
  border: solid 1.5px #00a152;
   min-width: 20%;
  max-width: 40%;
  height: 60px;
  background-color:#00a152;
  right: 15px;
  bottom: 15px;
  -webkit-animation-name: animate_in;
    -webkit-animation-duration: 0.3s;
    animation-name: animate_in;
    animation-duration: 0.3s;
}

#alert p {
	 
	padding:15px;
  position: relative;
  color: white;
  font-size:18px;

}
 


  
 
 
.frame{
	width: 600px;
	height: 350px;
	margin: 250px auto 0;
	position: relative;
	background: #435d77;
	border-radius:0 0 40px 40px; 
}
#button_open_envelope{
	width: 180px;
	height: 30px;
	position: absolute;
	z-index: 311;
	top: 250px;
	left: 208px;
	border-radius: 10px;
	color: #fff;
	font-size: 26px;
	padding:15px 0; 
	border: 2px solid #fff;
	transition:.3s;
}
#button_open_envelope:hover{
	background: #FFf;
	color: #2b67cb;
	transform:scale(1.1);
	transition:background .25s, transform .5s,ease-in;
	cursor: pointer;
}
.message{
	position: relative;
	width: 580px;
	min-height:300px;
	height: auto;
	background: #fff;
	margin: 0 auto;
	top: 30px;
	box-shadow: 0 0 5px 2px #333;
	transition:2s ease-in-out;
	transition-delay:1.5s;
	z-index: 300;
}
.left,.right,.top{width: 0;	height: 0;position:absolute;top:0;z-index: 310;}
.left{	
	border-left: 300px solid #7bdfa5;
	border-top: 160px solid transparent;
	border-bottom: 160px solid transparent;
}
.right{	
	border-right: 300px solid #7bdfa5;
	border-top: 160px solid transparent;
	border-bottom: 160px solid transparent;;
	left:300px;
}
.top{	
	border-right: 300px solid transparent;
	border-top: 200px solid #27ae60;
	border-left: 300px solid transparent;
	transition:transform 1s,border 1s, ease-in-out;
	transform-origin:top;
	transform:rotateX(0deg);
	z-index: 500;
}
.bottom{
	width: 600px;
	height: 190px;
	position: absolute;
	background: #187b43;
	top: 160px;
	border-radius:0 0 30px 30px;
	z-index: 310; 
}

.open{
	transform-origin:top;
	transform:rotateX(180deg);
	transition:transform .7s,border .7s,z-index .7s ease-in-out;
	border-top: 200px solid #2c3e50;
	z-index: 200;
}
.pull{
	-webkit-animation:message_animation 2s 1 ease-in-out;
	animation:message_animation 2s 1 ease-in-out;
	-webkit-animation-delay:.9s;
	animation-delay:.45s;
	transition:1.5s;
	transition-delay:1s;
	z-index: 350;
}
#name,#email,#phone,#messarea,#send{
	margin: 0;
	padding: 0 0 0 10px;
	width: 570px;
	height:40px;
	float: left;
	display: block;
	font-size: 18px;
	color: #2b67cb;
	border:none;
	border-bottom:1px solid #bdbdbd;
	letter-spacing: normal;
}
#messarea{
	height: 117px;
	width: 560px;
	overflow: auto;
	border:none;
	padding: 10px;
}
#send{ 
	width:   580px;
	padding: 0;	
	border:  none;
	cursor:  pointer;
	background: #7CB342;
	color: #fff;
	transition:.35s;
	letter-spacing: 1px;
}
#send:hover{background:tomato;transition:.35s;}

::-moz-placeholder{color: #7CB342;font-family: 'Ubuntu';font-size: 20px;opacity: 1;} 
::-webkit-input-placeholder {color: #7CB342; font-family: 'Ubuntu';font-size: 20px;}
*:focus {outline: none;}
input:focus:invalid,textarea:focus:invalid {
 /* when a field is considered invalid by the browser */
    background: #fff url(images/invalid.png) no-repeat 98% center;
    box-shadow: 0 0 5px #d45252;
    border:1px solid #b03535;
}
input:required:valid,textarea:required:valid { 
	/* when a field is considered valid by the browser */
    background: #fff url(images/valid.png) no-repeat 98% center;
    box-shadow: 0 0 5px #5cd053;
    border-color: #28921f;
}


@-webkit-keyframes message_animation {
	0%{
		transform:translatey(0px);
		z-index: 300;
		transition: 1s ease-in-out;
	}
	50%{
		transform:translatey(-340px);
		z-index: 300;
		transition: 1s ease-in-out;
	}
	51%{
		transform:translatey(-340px);
		z-index: 350;
		transition: 1s ease-in-out;
	}
	100%{
		transform:translatey(0px);
		z-index: 350;
		transition: 1s ease-in-out;
	}
}
@keyframes message_animation {
	0%{
		transform:translatey(0px);
		z-index: 300;
		transition: 1s ease-in-out;
	}
	50%{
		transform:translatey(-340px);
		z-index: 300;
		transition: 1s ease-in-out;
	}
	51%{
		transform:translatey(-340px);
		z-index: 350;
		transition: 1s ease-in-out;
	}
	100%{
		transform:translatey(0px);
		z-index: 350;
		transition: 1s ease-in-out;
	}
}
 </style>
  <section id="empolyee" class="pricing-area ">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <div class="section-title text-center pb-25">
                        <h3 class="title"> خدمة الموظفين
                        </h3>
                        <p class="text"> لدينا موظفين مختصين بتعبئة الطلبات الخاصة و بأرخص الاسعار </p>
                    </div>  
                </div>
            </div> 
       
<form id="msform" class="ajaxformm">
      
			  
	<fieldset>
		<h2 class="fs-title">اختر الموظف</h2>
	 
			<!-- <input type="number"  dir="rtl" name="user_id">  -->
					 <input  type="number"  dir="rtl" class="hide" name="user_id" value = "{{ Auth::user()->id }}"   > 

  		  <!-- <input type="text"  dir="rtl" name="namme"    placeholder="الاسم">  -->
	 
 <select  name="namme" id="use" >
 	 @foreach($users as $users)
  <option value="{{$users->name}} " >{{$users->name}} </option>
    @endforeach

</select>  
<br>
<input type="button" name="next" class="next action-button" value="التالي" />
	</fieldset>
	<fieldset>
		<h2 class="fs-title">اختر الخدمة</h2>
	 
			<!-- <input type="number"  dir="rtl" name="user_id">  -->
 
  	<!-- <input type="text"  dir="rtl" name="type"   placeholder="الخدمة"> -->
  <select  name="type" id="sel" >
  <option value="اخراج قيد" >اخراج قيد </option>
  <option value="سند امانة">سند امانة</option>
  <option value="مصدقة مدرسية">مصدقة مدرسية</option>

</select>  </br></br>
 
 

  <div id="alert" class="animate_in">

    <p id="message">تم ارسال طلبك</p>

  </div>
  
 
          	<button type="button "  id="button"  class="action-button  save-dataa" value="حفظ" >حفظ</button>
 

	</fieldset>
 
      
    
</form>
<br> </br><br> </br> <br> </br>  
 <div class="col-lg-12 col-md-12">
                    <div class="section-title text-center pb-25">
                         
                        <p class="text">  ارسل ايميل</p>
                    </div>  
                </div>
<form id="msform" class="ajaxformm">
      
			  
	<fieldset>
	  	<div class = "frame">
<div id = "button_open_envelope">
				ايميل
			</div>
			<div class = "message">
				<form method="post" action="contact.php">
					<input type="text" name="name" id="name" placeholder=" الاسم " required>
			
					<input type="email" name="email" id="email" placeholder=" الايميل " required pattern="^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$">
				
 				
					<textarea name="message"  id="messarea" placeholder="الرسالة " required></textarea>
			
					<input type="text" name="address" id="address" style="display: none;">
 		<button type="submit"  id="button"  class="action-button  save-dataa" value="ارسل" >ارسل</button>

				</form>
			</div>
			<div class = "bottom"></div>			
			<div class = "left"></div>
			<div class = "right"></div>
			<div class = "top"></div>
			<script src="js/script.js"></script>
		</div>
 
 	</fieldset>
	 
 
      <script type="text/javascript">
  (function() {
    var options = {
      whatsapp: "+91-9067201000", // WhatsApp number
      call: "+91-9067201000", // Call phone number
      call_to_action: "تواصل معنا", // Call to action
      button_color: "#FF6550", // Color of button
      position: "right", // Position may be 'right' or 'left'
      order: "whatsapp,call", // Order of buttons
    };
    var proto = document.location.protocol,
      host = "whatshelp.io",
      url = proto + "//static." + host;
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = url + '/widget-send-button/js/init.js';
    s.onload = function() {
      WhWidgetSendButton.init(host, proto, options);
    };
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
  })();
</script>
    
</form>
         </div>  

          
    </section>
     </br> </br>  </br> </br> </br> </br> </br> </br> </br>   </br> </br>   </br>  

<script>
 $(document).ready(function(){
	$('.frame').click(function(){
		$('.top').addClass('open');
		$('.message').addClass('pull');
	})
});
 //uses classList, setAttribute, and querySelectorAll
//if you want this to work in IE8/9 youll need to polyfill these
(function(){
	var d = document,
	accordionToggles = d.querySelectorAll('.js-accordionTriggerr'),
	setAria,
	setAccordionAria,
	switchAccordion,
  touchSupported = ('ontouchstart' in window),
  pointerSupported = ('pointerdown' in window);
  
  skipClickDelay = function(e){
    e.preventDefault();
    e.target.click();
  }

		setAriaAttr = function(el, ariaType, newProperty){
		el.setAttribute(ariaType, newProperty);
	};
	setAccordionAria = function(el1, el2, expanded){
		switch(expanded) {
      case "true":
      	setAriaAttr(el1, 'aria-expanded', 'true');
      	setAriaAttr(el2, 'aria-hidden', 'false');
      	break;
      case "false":
      	setAriaAttr(el1, 'aria-expanded', 'false');
      	setAriaAttr(el2, 'aria-hidden', 'true');
      	break;
      default:
				break;
		}
	};
//function
switchAccordion = function(e) {
  console.log("triggered");
	e.preventDefault();
	var thisAnswer = e.target.parentNode.nextElementSibling;
	var thisQuestion = e.target;
	if(thisAnswer.classList.contains('is-collapsed')) {
		setAccordionAria(thisQuestion, thisAnswer, 'true');
	} else {
		setAccordionAria(thisQuestion, thisAnswer, 'false');
	}
  	thisQuestion.classList.toggle('is-collapsed');
  	thisQuestion.classList.toggle('is-expanded');
		thisAnswer.classList.toggle('is-collapsed');
		thisAnswer.classList.toggle('is-expanded');
 	
  	thisAnswer.classList.toggle('animateIn');
	};
	for (var i=0,len=accordionToggles.length; i<len; i++) {
		if(touchSupported) {
      accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
    }
    if(pointerSupported){
      accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
    }
    accordionToggles[i].addEventListener('click', switchAccordion, false);
  }
})();
/* 
Orginal Page: http://thecodeplayer.com/walkthrough/jquery-multi-step-form-with-progress-bar 

*/
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})

</script>
<script>

  $(".save-dataa").click(function(event){
      event.preventDefault();
      let user_id = $("input[name=user_id]").val();
	  let namme = $("#use option:selected").val();
    //  let type = $("input[name=type]").val();
      let type = $("#sel option:selected").val();
      let _token   = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        url: "/order",
        type:"POST",
        data:{

        user_id :user_id ,
	    name :namme,
        type :type,
         
          _token: _token
        },
        success:function(response){
          console.log(response);
          if(response) {
            $('.success').text(response.success);
            $(".ajaxformm")[0].reset();
          }
        },
       });
  });
  document.addEventListener("click", function(e) {
  
  var button = document.getElementById("button");
  var alert = document.getElementById("alert");
  var message = document.getElementById("message");
  
  if (e.target == button) {
    
    document.getElementById('alert').style.display = "block";
    
  }
  
  else if (e.target == alert || e.target == message) {
    
    document.getElementById('alert').style.display = "none";
    
  }
  
});
</script>