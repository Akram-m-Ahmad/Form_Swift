<style>
 
.card {
  
  position: relative;
  border-radius: 6px;
  overflow: hidden;
  background-color: #fafafa;
  border: 1px solid grey;
  transition: all 0.3s ease-in-out;
  
}

.card .card-img {
  height: auto;
  width: 100%;
  background-color:#00a152;
  position: relative;
  background-size: cover;
  background-position: center top;
}

.card:hover {
  transition: all .35s;
  transform: translate3d(0, -5px, 0);
}

.card .card-content span {
  margin-bottom: 0.5rem;
  position: relative;
  display: block;
}

.card svg {
  position: absolute;
  top: 0;
}

.card .card-content {
  width: 100%;
  overflow-x: hidden;
  padding: 10px 20px 30px 20px;
  position: relative;
  overflow-y: hidden;
}

.card .card-content .bttn {
  display: inline-block;
  padding: 7px 8px;
  text-align: center;
  border-radius: 3px;
  background-color: #39454b;
  text-decoration: none;
  color: #fff;
  transition: 300ms all;
}

.card .card-content .bttn:hover {
  background-color: #3e4c53;
  color: #fff;
}

.card .card-content p:first-of-type {
  margin-top: 0px;
  text-align: left;
}


</style>
 <section id="news" class="pricing-area ">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <div class="section-title text-center pb-25">
                        <h3 class="title">الاخبار
                        </h3>
                        <p class="text"> اخر الاخبار بمايتعلق بالقوانين الجديدة </p>
                    </div>   </div>   </div>   </div>  
                  
                
<div class="container">
  <div class="row mt-5 mb-5">
     @foreach($nnews as $nnew)
    <div class="col-xs-12 col-sm-12 col-md-4 d-flex align-items-stretch mb-3">
      <div class="card">
 <img class="card-img" src="assets/images/fs_ogo.png">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 449">
            <path
              d="M-6.84 362.89c28.01-16.38 76.91-39.23 136.86-38.95 14.79.07 54.58.26 97.9 16.84 11.73 4.49 21.07 9.17 40 14.74 13.84 4.07 35.91 10.42 64.22 11.58 21.61.89 38.04-1.61 62.11-5.26 12.27-1.86 27.49-4.66 54.74-8.42 8.4-1.16 17.9-2.4 30.53-2.11 11.83.28 21.61 1.79 28.42 3.16l1.05 96.85-519-1.05c1.06-29.13 2.12-58.25 3.17-87.38z"
              fill="#fafafa" /></svg>
        <div class="card-content">
        	  
          <h4 dir="rtl">{{$nnew->name}}</h4>
          <span dir="rtl">{{$nnew->date}} </span>
          <p dir="rtl">{{$nnew->desc}}  </p>
         </div>
      </div>
    </div>

    
  @endforeach
  </div>
</div>
   </section>
<script>
   /* Demo purposes only */
  $(".hover").mouseleave(
    function () {
      $(this).removeClass("hover");
    }
  );
</script>









 