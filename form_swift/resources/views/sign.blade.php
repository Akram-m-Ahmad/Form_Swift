<!doctype html>
<html class="no-js" lang="ar">

<head>
               <link rel="icon" href="assets/images/fs_ogo.png" type="image/x-icon"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--====== Title ======-->
    <title>Form Swift</title>
    </head>

<style>

@import url("https://fonts.googleapis.com/earlyaccess/droidarabicnaskh.css");

* {
	box-sizing: border-box;
}

body {
	background: #f6f5f7;
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
 font-family:'Droid Arabic Naskh', serif;
	height: 100vh;
	margin: -20px 0 50px;
}

h1 {
	font-weight: bold;
	margin: 0;
}

h2 {
	text-align: center;
}

p {
	font-size: 14px;
	font-weight: 100;
	line-height: 20px;
	letter-spacing: 0.5px;
	margin: 20px 0 30px;
}

span {
	font-size: 12px;
}

a {
	color: #333;
	font-size: 14px;
	text-decoration: none;
	margin: 15px 0;
}

button {
	border-radius: 20px;
	border: 1px solid #00a152;
	background-color:#00a152;
	color: #FFFFFF;
	font-size: 12px;
	font-weight: bold;
	padding: 12px 45px;
	letter-spacing: 1px;
	text-transform: uppercase;
	transition: transform 80ms ease-in;
}

button:active {
	transform: scale(0.95);
}

button:focus {
	outline: none;
}

button.ghost {
	background-color: transparent;
	border-color: #FFFFFF;
}

form {
	background-color: #FFFFFF;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	padding: 0 50px;
	height: 100%;
	text-align: center;
}

input {
	background-color: #eee;
	border: none;
	padding: 12px 15px;
	margin: 8px 0;
	width: 100%;
}

.container {
	background-color: #fff;
	border-radius: 10px;
  	box-shadow: 0 14px 28px rgba(0,0,0,0.25), 
			0 10px 10px rgba(0,0,0,0.22);
	position: relative;
	overflow: hidden;
	width: 768px;
	max-width: 100%;
	min-height: 480px;
}

.form-container {
	position: absolute;
	top: 0;
	height: 100%;
	transition: all 0.6s ease-in-out;
}

.sign-in-container {
	left: 0;
	width: 50%;
	z-index: 2;
}

.container.right-panel-active .sign-in-container {
	transform: translateX(100%);
}

.sign-up-container {
	left: 0;
	width: 50%;
	opacity: 0;
	z-index: 1;
}

.container.right-panel-active .sign-up-container {
	transform: translateX(100%);
	opacity: 1;
	z-index: 5;
	animation: show 0.6s;
}

@keyframes show {
	0%, 49.99% {
		opacity: 0;
		z-index: 1;
	}
	
	50%, 100% {
		opacity: 1;
		z-index: 5;
	}
}

.overlay-container {
	position: absolute;
	top: 0;
	left: 50%;
	width: 50%;
	height: 100%;
	overflow: hidden;
	transition: transform 0.6s ease-in-out;
	z-index: 100;
}

.container.right-panel-active .overlay-container{
	transform: translateX(-100%);
}

.overlay {
	 background-color:#00a152;
	background-size: cover;
	background-position: 0 0;
	color: #FFFFFF;
	position: relative;
	left: -100%;
	height: 100%;
	width: 200%;
  	transform: translateX(0);
	transition: transform 0.6s ease-in-out;
}
img{
    margin-top:-72px;
}
.container.right-panel-active .overlay {
  	transform: translateX(50%);
}

.overlay-panel {
	position: absolute;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	 
	text-align: center;
	top: 0;
	height: 100%;
	width: 50%;
	transform: translateX(0);
	transition: transform 0.6s ease-in-out;
}

.overlay-left {
	transform: translateX(-20%);
}

.container.right-panel-active .overlay-left {
	transform: translateX(0);
}

.overlay-right {
	right: 0;
	transform: translateX(0);
}
 
    
    

.container.right-panel-active .overlay-right {
	transform: translateX(20%);
}

.social-container {
	margin: 20px 0;
}

.social-container a {
	border: 1px solid #DDDDDD;
	border-radius: 50%;
	display: inline-flex;
	justify-content: center;
	align-items: center;
	margin: 0 5px;
	height: 40px;
	width: 40px;
}
  
    
.inputContainer i {
   position: absolute;
}
.inputContainer {
   width: 100%;
   margin-bottom: 10px;
}
.icon {
   padding-top: 20px;
   padding-left:10px;
   color: rgb(49, 0, 128);
   width: 70px;
   text-align: left;
}
.Field {
   width: 100%;
   padding: 10px;
   text-align: center;
   font-size: 20px;
   font-weight: 500;
}
 
 
</style>
<body>
 <div class="container" id="container">
	<div class="form-container sign-up-container">
		<form method="post"  action="{{ route('register') }}">
		  {{ csrf_field() }}
			<h1>حساب جديد</h1>
			<div class="social-container">
			 
			</div> 
 			<input dir="rtl" name="name" type="text" placeholder="الاسم" />
			<input dir="rtl" name="email" type="email" placeholder="ايميل" />
			<div class="inputContainer">
			<input dir="rtl" name="password" id="pwdd"  type="password" placeholder="كلمة المرور" />
				 		 <i class="fa fa-eye icon" aria-hidden="true"  type="button" id="eyee"></i>

		</div>
			<button type="submit" >حساب جديد</button>
 		</form>
	</div>
	<div class="form-container sign-in-container">
		<form method="post"  action="{{ route('login') }}">
		 {{ csrf_field() }}
			<h1>تسجيل دخول</h1>
			<div class="social-container">
			 
			</div>
			<span> </span>
	  
			<input dir="rtl" name="email"   type="email" placeholder="ايميل" />
<div class="inputContainer">
			
			<input dir="rtl"    name="password"  id="pwd"  type="password" placeholder="كلمة المرور" />
		 		 <i class="fa fa-eye icon" aria-hidden="true"  type="button" id="eye"></i>


		 </div>
	 	 
       
			<a href="#">نسيت كلمة المرور ؟</a>
			<button type="submit">تسجيل دخول</button>
			 
		</form>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				    <img  src="assets/images/fs_ogo.png">
				<button class="ghost" id="signIn">تسجيل دخول</button>
			</div>
			<div class="overlay-panel overlay-right">
				     <img src="assets/images/fs_ogo.png">

				<button class="ghost" id="signUp">حساب جديد</button>
			</div>
          
	
		</div>
	</div>
</div>

 
<script>
const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});
function show() {
    var p = document.getElementById('pwd');
    p.setAttribute('type', 'text');
}

function showw() {
    var p = document.getElementById('pwdd');
    p.setAttribute('type', 'text');
}

function hide() {
    var p = document.getElementById('pwd');
    p.setAttribute('type', 'password');
}
function hidee() {
    var p = document.getElementById('pwdd');
    p.setAttribute('type', 'password');
}

var pwShown = 0;
var pwShownn = 0;

document.getElementById("eye").addEventListener("click", function () {
    if (pwShown == 0) {
        pwShown = 1;
        show();
    } else {
        pwShown = 0;
        hide();
    }
}, false);

document.getElementById("eyee").addEventListener("click", function () {
    if (pwShownn == 0) {
        pwShownn = 1;
        showw();
    } else {
        pwShownn = 0;
        hidee();
    }
}, false);


</script>
</body>
</html>