 

<meta charset="utf-8">

   <script src="https://cdn.bootcss.com/html2pdf.js/0.9.1/html2pdf.bundle.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.1/html2pdf.bundle.min.js"></script>
 
  

   <form id="msform"  method="post" class="AAA" action="{{ route('ekhraag') }}">
 @csrf
              @method('POST')  
			  
	<fieldset>
		<h2 class="fs-title">الاسم</h2>
	 
			<!-- <input type="number"  dir="rtl" name="user_id">  -->
					 <input  type="number" class="hide" dir="rtl" name="user_id" value = "{{ Auth::user()->id }}"   > 

  		<input type="text"  dir="rtl" name="name"   placeholder="الاسم">
 		<input type="text"  dir="rtl" name="sure"   placeholder="النسبة">

<input type="button" name="next" class="next action-button" value="التالي" />
	</fieldset>
    <fieldset>
		<h2 class="fs-title">اسم الاب والام</h2>
 		<input type="text"  dir="rtl" name="father"   placeholder="اسم الاب">
          		<input type="text"  dir="rtl" name="mother"   placeholder="اسم الام">

         	<input type="button" name="previous" class="previous action-button" value="السابق" />
<input type="button" name="next" class="next action-button" value="التالي" />
	</fieldset>
    <fieldset>
		<h2 class="fs-title">الامانة ورقم القيد</h2>
 		<input type="text"  dir="rtl" name="amana"   placeholder="الامانة">
          		<input type="number"  dir="rtl" name="num_quid"   placeholder="رقم القيد">

         	<input type="button" name="previous" class="previous action-button" value="السابق" />
<input type="button" name="next" class="next action-button" value="التالي" />
	</fieldset>
    <fieldset>
		<h2 class="fs-title">محل وتاريخ الولادة</h2>
 		<input type="date"  dir="rtl" name="DOB"   placeholder="تاريخ الولادة">
          		<input type="text"  dir="rtl" name="reg"   placeholder=" الدين">

         	<input type="button" name="previous" class="previous action-button" value="السابق" />
<input type="button" name="next" class="next action-button" value="التالي" />
	</fieldset>
    <fieldset>
		<h2 class="fs-title"> الرقم الوطني </h2>
 		<input type="number"  dir="rtl" name="num_id"   placeholder="الرقم الوطني">
          		<input type="text"  dir="rtl" name="quantity"   placeholder=" الجنس">

         	<input type="button" name="previous" class="previous action-button" value="السابق" />
<input type="button" name="next" class="next action-button" value="التالي" />
	</fieldset>
     <fieldset>
		<h2 class="fs-title"> الوضع العائلي </h2>
 		<input type="text"  dir="rtl" name="status"   placeholder=" الوضع العائلي">
          		<textarea type="text"  dir="rtl" name="notes"   placeholder="ملاحظات">
</textarea>
         	<input type="button" name="previous" class="previous action-button" value="السابق" />
 		<input type="button" name="next" class="next action-button" value="التالي" />
 
	</fieldset>
      <fieldset>
		  <button type="submit"  class="action-button"> save</button> 
 	      <input type="button" name="next" class="next action-button" value="PDF" />
	</fieldset>
	    <fieldset>
		<div id="element-to-print">
 		  <div class="row">
		    <div class="col-sm-4" style="background-color:lavenderblush;">{{$data->sure}}</div>
			 <div class="col-sm-2" style="background-color:lavender;">النسبة</div>
			 <div class="col-sm-4" style="background-color:lavenderblush;">{{$data->name}}</div>
           <div class="col-sm-2" style="background-color:lavender;">الاسم</div>
		  </div>
		  <hr>
		   <div class="row">
		    <div class="col-sm-4" style="background-color:lavenderblush;">{{$data->mother}}</div>
			 <div class="col-sm-2" style="background-color:lavender;">اسم الام</div>
			 <div class="col-sm-4" style="background-color:lavenderblush;">{{$data->father}}</div>
           <div class="col-sm-2" style="background-color:lavender;">اسم الاب</div>
		  </div>
		  <hr>
        <div class="row">
		    <div class="col-sm-2" style="background-color:lavenderblush;">{{$data->num_quid}}</div>
			 <div class="col-sm-2" style="background-color:lavender;">رقم القيد</div>
			 <div class="col-sm-2" style="background-color:lavenderblush;">{{$data->amana}}</div>
           <div class="col-sm-2" style="background-color:lavender;">الامانة</div>
		   <div class="col-sm-2" style="background-color:lavenderblush;">{{$data->DOB}}</div>
			 <div class="col-sm-2" style="background-color:lavender;">تاريخ الولادة</div>
		  </div>
		  <hr>
         <div class="row">
		    
			 <div class="col-sm-2" style="background-color:lavenderblush;">{{$data->reg}}</div>
           <div class="col-sm-2" style="background-color:lavender;">الدين</div>
		   <div class="col-sm-2" style="background-color:lavenderblush;">{{$data->num_id}}</div>
           <div class="col-sm-2" style="background-color:lavender;"> الرقم الوطني </div>
		   <div class="col-sm-2" style="background-color:lavenderblush;">{{$data->status}}</div>
			 <div class="col-sm-2" style="background-color:lavender;"> الوضع العائلي</div>
		  </div>
		  </div>
		  <hr>
		  <div class="row">
 			 <div class="col-sm-10" style="background-color:lavenderblush;">{{$data->notes}}</div>
           <div class="col-sm-2" style="background-color:lavender;">  ملاحظات</div>
		  </div>
 
  <input type="button" class="html2PdfConverter action-button" onclick="createPDF()" value="PDF"> 

  <!-- <input type="button" id="create_pdf" value="Generate PDF">   -->
	</fieldset>
 
</form>
 </br> </br>  </br> </br> </br> </br> </br> </br> </br>   </br> </br>   </br>  



<script>
    function createPDF() {
      var element = document.getElementById('element-to-print');
      html2pdf(element, {
        margin: 1,
        padding: 0,
        filename: 'myfile.pdf',
        image: {
          type: 'jpeg',
          quality: 1
        },
        html2canvas: {
          scale: 2,
          logging: true
        },
        jsPDF: {
          unit: 'in',
          format: 'A2',
          orientation: 'P'
        },
        class: createPDF
      });
    };
    
  </script>
 