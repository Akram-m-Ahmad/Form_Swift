<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSchool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_school', function (Blueprint $table) {
            $table->bigIncrements('id');
           // $table->timestamps();
            $table->integer('user_id');
            $table->String('name_school');
            $table->String('name_student');
            $table->date('DOB'); 
            $table->integer('num_study');
            $table->integer('num_session');
            $table->String('name_degree');
            $table->String('time_trans');
            $table->String('address');
            $table->date('date-out'); 
            $table->date('date-now'); 
            $table->string('notes');                 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_school');
    }
}
