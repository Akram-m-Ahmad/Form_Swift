<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEkhrag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_ekhrag', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->timestamps();
            $table->integer('user_id');
            $table->string('name');
            $table->string('sure');
            $table->string('father');
            $table->string('mother');
            $table->string('amana');
            $table->integer('num_quid');
            $table->date('DOB');
            $table->string('reg');
            $table->integer('num_id');
            $table->string('status');
            $table->string('notes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_ekhrag');
    }
}
