<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSanat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_sanat', function (Blueprint $table) {
            $table->bigIncrements('id');
           // $table->timestamps();
            $table->integer('user_id');
            $table->string('name');
            $table->string('father');
            $table->string('mother');            
            $table->date('DOB');            
            $table->integer('num_id');
            $table->date('date_id');
            $table->date('place_date');
            $table->string('address');
            $table->integer('money');
            $table->date('date_now');
            $table->string('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_sanat');
    }
}
