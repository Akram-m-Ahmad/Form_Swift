<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sanat extends Model
{
         protected $table = "table_sanat";

    public $timestamps = false;
    protected $fillable = [
         'user_id', 
         'name',
         'father',
         'mother',           
         'DOB',            
         'num_id',
         'date_id',
         'place_date',
         'address',
         'money',
         'date_now',
         'notes',
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
