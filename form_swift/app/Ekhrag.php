<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ekhrag extends Model
{
    //
     protected $table = "table_ekhrag";
     public $timestamps = false;
 
 
    protected $fillable = [
         'user_id', 
         'name',
         'sure', 
         'father', 
         'mother',
         'amana',
         'num_quid',
         'DOB',
         'reg',
         'num_id',
         'status',
         'notes',
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
