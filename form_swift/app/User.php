<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


       public function ekhrag()
    {
        return $this->hasMany(Ekhrag::class, 'user_id', 'id');
    }
    public function sanat()
    {
        return $this->hasMany(Sanat::class, 'user_id', 'id');
    }
     public function school()
    {
        return $this->hasMany(School::class, 'user_id', 'id');
    }
     public function news()
    {
        return $this->hasMany(News::class, 'user_id', 'id');
    }
public function order()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }

}
