<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
      protected $table = "table_news";

     public $timestamps = false;
    protected $fillable = [
              
            'name',
            'date',
            'desc',
            'user_id',
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
