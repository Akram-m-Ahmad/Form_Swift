<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Carbon\Carbon;
//use JWTAuth;
use View;
use Auth;
class NewsController extends Controller
{
    //all news
    public function index()
    {
 if(Auth::check()){
         $news=News::all();
                             return view('admin.newsadmin' );
 }
  
        // return view('admin.newsadmin',[ 'news' => $news ]);
         /// return News::all();
            // return redirect()->route('login')->with('news',$news);
    //return    View::share('news',News::all());
//return view('news.list')->with('news', $news) ;
    //return view('index', compact('news'));


    }
 //find news by id
    public function find($id){
        return News::findOrFail($id);
    }

    //Create a news
    public function store(Request $request){   
       return News::create($request->all());
    }

    //  update a News
     public function update(Request $request, $id){
         $news = News::findOrFail($id);
         
 
         if (!$news) {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, News with id ' . $id . ' cannot be found.'
             ], 400);
         }
         

         $updated = $news->fill($request->all())->save();
         

         if ($updated) {
             return response()->json([
                 'success' => true,
                 'News' => $news

             ]);
         } else {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, News could not be updated.'
             ], 500);
         }
         
     }


     //delete a News
     public function destroy($id)
     {
            
        $newss= News::find($id);
        $newss->delete();
        return back();

 
 
}
}
