<?php

namespace App\Http\Controllers;
use App\Order;
use Carbon\Carbon;
use JWTAuth; 

use Illuminate\Http\Request;

class OrderController extends Controller
{
     public function getDataO($id = 0){
    // get records from database

    if($id==0){
     // $arr['data'] = Sanat::orderBy('id', 'asc')->get(); 
        $arr['data'] = Order::orderBy('id', 'DESC')->first();

    }else{
      $arr['data'] = Order::where('id', $id)->first();
    }
    echo json_encode($arr);
    exit;
  }
    //
      //Get All Order
     public function index() {
        return Order::all(); 
    }

    //find Order by id
    public function find($id){
        return Order::findOrFail($id);
    }

    //Create a Order
    public function store(Request $request){   
        return Order::create($request->all());
    }

    //  update a Order
     public function update(Request $request, $id){
         $order = Order::findOrFail($id);
         
 
         if (!$order) {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, Order with id ' . $id . ' cannot be found.'
             ], 400);
         }
         

         $updated = $order->fill($request->all())->save();
         

         if ($updated) {
             return response()->json([
                 'success' => true,
                 'Order' => $order

             ]);
         } else {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, Order could not be updated.'
             ], 500);
         }
         
     }


     //delete a Order
     public function destroy($id)
     {
         $order = Order::findOrFail($id);
 
         if (!$order) {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, Order with id ' . $id . ' cannot be found.'
             ], 400);
         }
 
         if ($order->delete()) {
             return response()->json([
                 'success' => true,
                 
             ]);
         } else {
             return response()->json([
                 'success' => false,
                 'message' => 'Order could not be deleted.'
             ], 500);
         }
     }

 
 
}
