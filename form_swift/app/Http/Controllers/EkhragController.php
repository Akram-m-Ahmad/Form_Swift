<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ekhrag;
 use App\News;
use Carbon\Carbon;
 
use Validator;
use Auth;
use App\User;
class EkhragController extends Controller
{
    //
    //all Ekhrag
    public function index()
    {
        $ekh= Ekhrag::all();
        return $ekh;
// return view('pdf',['ekh' => 
//                   $ekh]); 
    }
 //find Ekhrag by id
    public function show($id){
        $data=Ekhrag::findOrFail($id);
         
   // return view('admin.packages.show')->with('package', $package);
    }
     public function last(){
         return   $last_row = Ekhrag::orderBy('id', 'DESC')->first();
    
    }
 
    //Create a Ekhrag
    public function store(Request $request){ 

         
       $validator = Validator::make($request->all(), [
            //  'user_id'=>'required', 
         'name'=>'required',
         'sure'=>'required', 
         'father'=>'required', 
         'mother'=>'required',
         'amana'=>'required',
         'num_quid'=>'required',
         'DOB'=>'required',
         'reg'=>'required',
         'num_id'=>'required',
         'status'=>'required',
         'notes'=>'required'
             
        ]);
       
         $data=Ekhrag::create([
            'user_id'=>$request->get('user_id')   , 
          'name'=> $request->get('name'),
         'sure'=> $request->get('sure'), 
         'father'=> $request->get('father'), 
         'mother'=> $request->get('mother'),
         'amana'=> $request->get('amana'),
         'num_quid'=> $request->get('num_quid'),
         'DOB'=> $request->get('DOB'),
         'reg'=> $request->get('reg'),
         'num_id'=> $request->get('num_id'),
         'status'=> $request->get('status'),
         'notes'=> $request->get('notes')
             
        ]);
      
               $data->save(); 
               $lastInsertedId = $data->id; 
//$ekh = [Ekhrag::all()];
//var_dump($ekh);
      $nnews = News::all();
  //   var_dump($news);
//return Ekhrag::create($request->all());
    // return response()->json(compact('data'),201);
     // return view('forms.ekhrag');
     //eturn redirect()->route('index');
                 return redirect()->route('index');
              $user = auth()->user()->id; 
 //return view('index', ['nnewss' => $nnews , 'user'=>$user ]);


    }


    //  update a Ekhrag
     public function update(Request $request, $id){
         $ekhrag = Ekhrag::findOrFail($id);
         
 
         if (!$ekhrag) {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry,Ekhrag with id ' . $id . ' cannot be found.'
             ], 400);
         }
         

         $updated = $ekhrag->fill($request->all())->save();
         

         if ($updated) {
             return response()->json([
                 'success' => true,
                 'Ekhrag' => $ekhrag

             ]);
         } else {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, Ekhrag could not be updated.'
             ], 500);
         }
         
     }


     //delete a Ekhrag
     public function destroy($id)
     {
         $ekhrag = Ekhrag::findOrFail($id);
 
         if (!$ekhrag) {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, Ekhrag with id ' . $id . ' cannot be found.'
             ], 400);
         }
 
         if ($ekhrag->delete()) {
             return response()->json([
                 'success' => true,
                 
             ]);
         } else {
             return response()->json([
                 'success' => false,
                 'message' => 'Ekhrag could not be deleted.'
             ], 500);
         }
     }

 
 
}
