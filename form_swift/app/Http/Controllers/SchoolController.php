<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use Carbon\Carbon;
use JWTAuth; 

class SchoolCotroller extends Controller
{
    //
      //Get All Schools
     public function index() {
        return School::all(); 
    }

    //find School by id
    public function find($id){
        return School::findOrFail($id);
    }

    //Create a School
    public function store(Request $request){   
        return School::create($request->all());
    }

    //  update a School
     public function update(Request $request, $id){
         $school = School::findOrFail($id);
         
 
         if (!$school) {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, School with id ' . $id . ' cannot be found.'
             ], 400);
         }
         

         $updated = $school->fill($request->all())->save();
         

         if ($updated) {
             return response()->json([
                 'success' => true,
                 'School' => $school

             ]);
         } else {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, School could not be updated.'
             ], 500);
         }
         
     }


     //delete a School
     public function destroy($id)
     {
         $school = School::findOrFail($id);
 
         if (!$school) {
             return response()->json([
                 'success' => false,
                 'message' => 'Sorry, School with id ' . $id . ' cannot be found.'
             ], 400);
         }
 
         if ($school->delete()) {
             return response()->json([
                 'success' => true,
                 
             ]);
         } else {
             return response()->json([
                 'success' => false,
                 'message' => 'School could not be deleted.'
             ], 500);
         }
     }

}
