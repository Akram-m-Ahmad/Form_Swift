<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
          protected $table = "table_order";

       public $timestamps = false;
    protected $fillable = [
            'name',
            'type',
             
             'user_id',
    ];
     public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
