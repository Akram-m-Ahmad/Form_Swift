<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    //
     public $timestamps = false;
    protected $fillable = [
         'user_id', 
            'name_school', 
            'name_student', 
            'DOB',  
            'num_study', 
            'num_session', 
            'name_degree', 
            'time_trans', 
            'address', 
            'date-out',  
            'date-now', 
            'notes',    
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
