<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
 Route::post('userregister', 'Auth\RegisterController@register');
 Route::post('userlogin', 'Auth\LoginController@login') ;
Route::get('/last', 'EkhragController@last');
Route::get('/getData/{id}','SanatController@getData');
  Route::post('/order','OrderController@store');
Route::delete('delnews/{id}', 'NewsController@destroy');

    Route::group(['middleware' => 'auth'], function () {
 Route::resource('news', 'NewsController');
     Route::post('userekhrag', 'EkhragController@store');

 });
Auth::routes();
 Auth::routes();