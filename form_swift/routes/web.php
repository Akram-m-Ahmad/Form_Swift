<?php
use Illuminate\Support\Facades\Auth;
use App\News;
use Illuminate\Support\Facades\Route;
use App\Ekhrag;
use App\Sanat;
use App\User;
use App\Order;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
 
  Route::post('userregister', 'Auth\RegisterController@register')->name('register');
  
 //Route::get('/home', 'HomeController@index')->name('home');
// Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

 Route::group(['middleware' => 'auth'], function () {
 Route::resource('news', 'NewsController');
   Route::post('userekhrag', 'EkhragController@store')->name('ekhraag');
   Route::get('sanat', 'SanatController@create');
Route::post('sanat', 'SanatController@store');
Route::post('order', 'OrderController@store');
Route::delete('delnews/{id}', 'NewsController@destroy') ;
Route::get('delnews/{id}', 'NewsController@destroy') ;

      Route::post('addnews', 'NewsController@store')->name('addnews');
Route::get('/ekhrag/{id}', 'EkhragController@show');
Route::get('/getData/{id}','SanatController@getData');
Route::get('/getData/{id}','SanatController@getData');
  Route::get('/getDataO/{id}','OrderController@getDataO');

 });
 
   
 
 Auth::routes();
 Auth::routes();
 
   
Route::get('/index', function () {
$nnews=News::all();
     $users=User::query()->Where('is_admin', '=', 1)->get();
        $data = Ekhrag::orderBy('id', 'DESC')->first();
 $dataS = Sanat::orderBy('id', 'DESC')->first();
   return view('index',[

    'nnews'=> $nnews
    ,'data'=>$data,
    'dataS'=>$dataS,
    'users'=>$users

  ]);
       
})->name('index') ;

Route::get('/admin', function () {

    $order = Order::all();
    $news=News::all();
     
   return view('admin.home',[

    'news'=> $news,
'order'=>$order
  ]);
       
})->name('admin') ;


Route::get('/home', function () {

    return view('home');
       
})  ;


   Route::get('/sign', function () {

    return view('sign');
       
  })->name('sign') ;
 

 
